//
//  ViewController.swift
//  BlueTracker
//
//  Created by zappycode on 6/28/17.
//  Copyright © 2017 Nick Walter. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CBCentralManagerDelegate {
    
    var centralManager : CBCentralManager?
    
    //to create a spinner
    let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    var names : [String] = []
    var RSSIs :[NSNumber] = []
    var uiid : [String] = []
    var perifiere: [CBPeripheral] = []
    var timer : Timer?
    var parentView: ViewController? = nil
    
    var refresher: UIRefreshControl = UIRefreshControl()

    @IBOutlet weak var tableView: UITableView!
    @objc func updateTable(){
        startScan()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        refresher.attributedTitle = NSAttributedString(string: "Pull to refresh")
        //use the funktion to the refresher
        refresher.addTarget(self, action: #selector(ViewController.updateTable), for: UIControlEvents.valueChanged)
        //to add the refresher to the table
        tableView.addSubview(refresher)
        
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
        
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
        startScan()
      // startTimer()
    }
    
    /*func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { (timer) in
            self.startScan()
        })
    }*/
    
    func startScan() {
        names = []
        RSSIs = []
        perifiere = []
        tableView.reloadData()
        centralManager?.stopScan()
        centralManager?.scanForPeripherals(withServices: nil, options: nil)
        refresher.endRefreshing()
    }
    
    // CBCentralManger Code
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if let name = peripheral.name {
            names.append(name)
        } else {
            names.append("Inget namn")
            
        }
        uiid.append(peripheral.identifier.uuidString)
        RSSIs.append(RSSI)
        
        
        perifiere.append(peripheral)
        //centralManager?.connect(peripheral, options: nil)
        //central.connect(peripheral, options: nil)
        
        self.tableView.reloadData()
       
    }
  
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
       
     
        activityIndicator.stopAnimating()
        
        UIApplication.shared.endIgnoringInteractionEvents()
        
        // pass reference to connected peripheral to parentview
        
        //peripheral.discoverServices(nil)
        // set manager's delegate view to parent so it can call relevant disconnect methods
        peripheral.delegate = self as? CBPeripheralDelegate
        //centralManager(centralManager!, didConnect: perifi)
        print("connected")
        print(peripheral)
       
        
        let alertVC = UIAlertController(title: "Connected", message: "Fantastiskt du är connected!", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            alertVC.dismiss(animated: true, completion: nil)
        })
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
        
       // print("Connected to " + peripheral.name!)
       
        
        //go to another view
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let perifi = perifiere[indexPath.row]
         centralManager?.connect(perifi, options: nil);
         print("inne inne på: \(String(describing: perifi))");
       
        
        //specify where we want the acticityIndicator to appear
        
        activityIndicator.center = self.view.center
        
        //when it is done we want to hide it activityIndicator
        
        activityIndicator.hidesWhenStopped = true
        
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        //to add out acitivtyIndicator to the view
        view.addSubview(activityIndicator)
        
        //start animating our controller so it spinns around
        activityIndicator.startAnimating()
        
        //to stop the hole app stop the user to interact with our app
        UIApplication.shared.beginIgnoringInteractionEvents()
        
    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            // Working
            startScan()
           //startTimer()
            print("Itworked")
        } else {
            // Not Working
            let alertVC = UIAlertController(title: "Bluetooth fungerar inte", message: "Kolla om din bluetooth är på.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                alertVC.dismiss(animated: true, completion: nil)
            })
            alertVC.addAction(okAction)
            present(alertVC, animated: true, completion: nil)
        }
    }
    
    
    
    // TableView Code
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "blueCell", for: indexPath) as? BlueTableViewCell {
            
            cell.nameLabel.text = names[indexPath.row]
            
            cell.rssiLabel.text = "RSSI: \(RSSIs[indexPath.row])"
            cell.uiidLable.text = uiid[indexPath.row]
            //cell.uuidLabel.text = uiid
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    

}

